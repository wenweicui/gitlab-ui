import examples from './examples';
import description from './toast.md';

export default {
  followsDesignSystem: false,
  description,
  examples,
};
